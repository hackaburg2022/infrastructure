# infrastructure

Infrastructure for KochChef with Elasticsearch and Kibana

## Getting started

```
docker-compose up -d
```

Starts Kibana on localhost:5601 and elasticsearch on localhost:9200
